#!/usr/bin/python3

# %%
"""Module importing"""

# %%
import os,sys
import asyncio
# import argparse
from functools import singledispatchmethod

import pandas as pd

# %%
"""Filtering class for a dataframe"""

# %%
class DfFilter:
    def __init__(self, df: pd.DataFrame):
        self.df: pd.DataFrame = df.copy()

    # Filters for tables like 'TABLA1'
    def to_normal(self):
        """Filter database to only normal products"""
        self.df = (self.df.loc[
                    (self.df['PRODUCTO_VAP']=='STD') &
                    (self.df['DESCRIPCION_PRODUCTO'].str.contains('^(GB|GR|HN|HB).{9}$', regex=True))
                ])
        return self

    def to_metropolitan(self):
        """Filter database to only the ones in metropolitan region"""
        codigos_metropolitanos = (101,111,121,122,123,131,132,133,151)
        self.df = (self.df.loc[self.df['CODIGO_SUCURSAL'].isin(codigos_metropolitanos)])
        return self

    def cols_from_description(self, encode: bool = True):
        """Expand data columns from information in the product description"""
        colnames: tuple = (
                "TIPO_CONCRETO", "RESISTENCIA_ESPECIFICADA",
                "NIVEL_CONFIANZA", "TAMAÑO_ARIDO", "CONO_SOLICITADO"
                )
        coltypes: tuple = ("string",) + ("int8",) * 4

        # Extract data from the description
        expanded_cols: pd.DataFrame = self.df['DESCRIPCION_PRODUCTO'].str\
                .extract(r'(..)(...)(..)(..)(..)')

        expanded_cols =  expanded_cols.set_axis(colnames, axis=1)\
                     .astype( dict( zip(colnames,coltypes) ) )

        # Remove original column and add expanded columns from it, with
        # correct column names and column types
        self.df = pd.concat( [self.df.drop(['DESCRIPCION_PRODUCTO'], axis=1), expanded_cols], axis=1)

        # Dummy columns from the concrete type
        if encode:
            self.df =  pd.get_dummies(self.df, columns=['TIPO_CONCRETO'], drop_first=True)

        return self

    # Filters for tables like 'TABLA2'
    def only_C_testing_type(self):
        """Filter to only testing of type 'C'"""
        self.df = self.df.loc[self.df['TIPO_ENSAYO'] == 'C']
        return self

    def remove_testless_samples(self):
        """Removes samples with no tests (that have (0|NA) and (0|NA)) """
        self.df = (self.df.loc[
                   ~( (self.df['R28_1'].isin(0,pd.NA)) & 
                      (self.df['R28_2'].isin(0,pd.NA)) )
                  ])
        return self

    def create_mean_column(self):
        """Creates column of day 28 as mean of the 2 samples"""
        # Sum samples
        self.df['R28'] = self.df['R28_1'] + self.df['R28_2']
        # If either sample wasn't '0', divide by 2 to get the mean
        # ** Thus, you need to have removed NAs first, TODO: remove this requirement
        self.df.loc[ ~((self.df['R28_1'] == 0) |
                       (self.df['R28_2'] == 0)), 'R28'
                   ] = self.df['R28']/2
        return self

# %%
"""CSV async reader class"""

# %%
class CSVReader:
    """
    Read CSV(s) passed as argument(s) to a pandas dataframe(s), asynchroniously,
    and deleting them after use

    As a context manager:
        async with CSVReader("file.csv") as df:
            print(df.columns)

    As a function generator:
        async for df in CSVReader("file1.csv", "file2.csv"):
            print(df.columns)
    """
    @singledispatchmethod
    def __init__(self,p1):
        raise TypeError(f"Parameter {p1} must be a filepath(s) of type str or list[str]")
    @__init__.register(str)
    def _(self, filepath: str):
        """Receive 1 filepath to read"""
        self.filepath = filepath
    @__init__.register(list)
    def _(self, filepaths: list[str]):
        """Receive filepaths to read"""
        self.filepaths: list[str] = filepaths

    # When used as a generator, yield dataframes
    async def __aiter__(self):
        """Returns async generator for reading csv databases"""
        for filepath in self.filepaths:
            task = asyncio.to_thread(pd.read_csv, filepath, low_memory=False )
            df = await task
            yield df
            del df  # Release memory when stop using previous dataframe

    # When used as a context manager, enter reading them, exit deleting them to release memory
    async def __aenter__(self):
        task = asyncio.to_thread(pd.read_csv, self.filepath, low_memory=False)
        self.df = await task
        return self.df
    async def __aexit__(self, type, value, traceback):
        del self.df

# %%
# async def amain(cmdargs: list = []):
async def amain():
    # args = await parse_cmdargs(cmdargs)

    databases: list[str] = [ os.path.join("..", "bases", f"CAA_AP_TABLA{_}.csv") for _ in range(1,8)]

    async with CSVReader(databases[0]) as df:
        print(df.shape)
        filter = DfFilter(df)
        df_filtered = filter.to_metropolitan().to_normal().cols_from_description().df
        # Filtrando dan 183491 filas

        print(df_filtered.iloc[:3,:])
        print(df_filtered.shape)

    async with CSVReader(databases[1]) as df:
        print(df.shape)
        filter = DfFilter(df)
        df_filtered = filter.only_C_testing_type().df
        print(df_filtered.shape)

# %%
# def main(cmdargs: list = []):
def main():

    if IN_JUPYTER:
        loop = asyncio.get_event_loop()
        asyncio.run_coroutine_threadsafe(amain(), loop)
    else:
        asyncio.run(amain())
        # asyncio.run(amain(cmdargs))

# %%
"""Entry point"""

# %%
if os.path.split(sys.argv[0])[-1] == "ipykernel_launcher.py":
    IN_JUPYTER = True
    main()
elif __name__ == "__main__":
    IN_JUPYTER = False
    main()








